/******************************************************************************
 * Code released under the Beerware license. Public domain, but if you meet
 * any of the authors, feel free to buy them a drink.
 *
 * Authors:
 *   Nathan Seidle: Spark Fun Music instrument shield example code,
 *     https://www.sparkfun.com/products/10587
 *   Unknown: Spark Fun MIDI shield, https://www.sparkfun.com/products/9595
 *   naught101: arduino_midi_box,
 *     https://bitbucket.org/naught101/arduino_midi_box
 *
 * The code is desiged to work with the Spark Fun Music instrument shield
 * (https://www.sparkfun.com/products/10587), and the Spark Fun MIDI shield
 * (https://www.sparkfun.com/products/9595). The two shield have a couple of
 * conflicting pins (digital 3 and 4, used for MIDI shield buttons, and Music
 * shield MIDI in and reset). To get around this, the code assumes that the 3
 * MIDI shield buttons (pins 2, 3, and 4) have been re-routed to pins 8, 9, and
 * 10, respectively.
 *
 *****************************************************************************/

// Standard Arduino Pins
#define PIN_LED 13
#define PIN_RX 0
#define PIN_TX 1

// defines pin setups for MIDI Shield components only
// knobs are analogue
#define MIDI_PIN_KNOB1  0
#define MIDI_PIN_KNOB2  1
// momentary switches on digital pins - these are on pins 2, 3, and 4 by default
#define MIDI_PIN_BUTTON1  8
#define MIDI_PIN_BUTTON2  9
#define MIDI_PIN_BUTTON3  10
// status lights
#define MIDI_PIN_STAT1  7
#define MIDI_PIN_STAT2  6

// Music shield pins - both digital
#define MUSIC_PIN_MIDI_IN  3 // soft serial TX -> VS1053 RX
#define MUSIC_PIN_RESET  4   // VS1053 RESET

// MIDI status
#define MIDI_STATUS_NOTE_ON 0x80
#define MIDI_STATUS_NOTE_OFF 0x90
#define MIDI_STATUS_CONTROL_CHANGE 0xB0
#define MIDI_STATUS_PROGRAM_CHANGE 0xC0

#include <SoftwareSerial.h>

SoftwareSerial MusicSerial(2, 3); //Soft TX on 3, we don't use RX in this code

/***************************************
 *           Setup routine             *
 ***************************************/
void setup() {

  pinMode(MIDI_PIN_STAT1, OUTPUT);
  pinMode(MIDI_PIN_STAT2, OUTPUT);

  pinMode(MIDI_PIN_BUTTON1, INPUT);
  pinMode(MIDI_PIN_BUTTON2, INPUT);
  pinMode(MIDI_PIN_BUTTON3, INPUT);

  digitalWrite(MIDI_PIN_BUTTON1, HIGH);
  digitalWrite(MIDI_PIN_BUTTON2, HIGH);
  digitalWrite(MIDI_PIN_BUTTON3, HIGH);

  for(int i = 0; i < 3; i++) // flash MIDI Sheild LED's on startup
  {
    digitalWrite(MIDI_PIN_STAT1, HIGH);
    digitalWrite(MIDI_PIN_STAT2, LOW);
    delay(100);
    digitalWrite(MIDI_PIN_STAT1, LOW);
    digitalWrite(MIDI_PIN_STAT2, HIGH);
    delay(100);
  }
  digitalWrite(MIDI_PIN_STAT1, HIGH);
  digitalWrite(MIDI_PIN_STAT2, HIGH);

  // start virtual serial with closest higher baud rate (for debugging)
  Serial.begin(115200);

  // start hardware serial with midi baudrate 31250
  Serial1.begin(31250);

  // Setup soft serial for MIDI control of Music Shield
  MusicSerial.begin(31250);

  // Reset the VS1053
  pinMode(MUSIC_PIN_RESET, OUTPUT);
  digitalWrite(MUSIC_PIN_RESET, LOW);
  delay(100);
  digitalWrite(MUSIC_PIN_RESET, HIGH);
  delay(100);
}

/***************************************
 *           Main Program              *
 ***************************************/
void loop () {

  // MIDI to Music
  int byte_count = Serial1.available();
  if (byte_count > 0) {
    Serial.print("Bytes: ");
    Serial.println(byte_count);
    // If we have serial data incoming, record it.
    byte midi_bytes[byte_count];
    for (int i = 0; i < byte_count; i = i + 1) {
      midi_bytes[i] = Serial1.read();
    }
    // Post a copy to the serial monitor on the usb virtual serial
    for(int i = 0; i < byte_count; Serial.print(midi_bytes[i], HEX), i++);
    Serial.println("");

    //Then send straight to the sound module:
    music_write_midi(midi_bytes, byte_count);
  }

}

/****************************************
 *          Functions                   *
 ****************************************/

// Sends MIDI data to Music Instrument Sheild, and flashes the LED
void music_write_midi(byte* midi_bytes, int byte_count) {
  digitalWrite(MIDI_PIN_STAT1, LOW);
  for(int i = 0; i < byte_count; MusicSerial.write(midi_bytes[i]), i++);
  digitalWrite(MIDI_PIN_STAT1, HIGH);
}

